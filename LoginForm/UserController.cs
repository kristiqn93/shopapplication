using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginForm
{
    public  class UserController
    {
        public static bool Login(string username, string password)
        {
            using (var db = new ShopStorageDatabase())
            {
                var user = db.Users.FirstOrDefault(u => u.Username == username);

                if (username.Length < 3 && password.Length < 3)
                {

                    MessageBox.Show("Characters must be more than 3 length !", "Minimum legth", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);

                    return false;
                }

                if (user != null)
                {
                    if (user.Password == password)
                    {


                        return true;

                    }
                    MessageBox.Show("Forgoten username or password", "User or Pass",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return false;
                }

                return false;
            }
        }


        public static Task<bool> LoginAsync(string username, string password)
        {
            return Task.Factory.StartNew(() => Login(username, password));
        }
    }
}