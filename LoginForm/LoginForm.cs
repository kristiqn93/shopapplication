﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LoginForm
{
    public partial class LoginForm : DevExpress.XtraEditors.XtraForm
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private async void  btnLogin_Click(object sender, EventArgs e)
        {
            var username = txtUsername.Text;
            var password = txtPassword.Text;

            var login = await UserController.LoginAsync(username, password);
            if (login)
            {
                var shop = new ShopForm();
                shop.Show();
                this.Hide();
            }
            else
            {
                txtUsername.Text = null;
                txtPassword.Text = null;
            }

        }


        private async void txtPassword_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                var username = txtUsername.Text;
                var password = txtPassword.Text;

                var login = await UserController.LoginAsync(username, password);
                if (login)
                {
                    using (var db = new ShopStorageDatabase())
                    {
                        var user = db.Users.FirstOrDefault(u => u.Username == username);

                        if (user.RIghts == "admin")
                        {
                            var shop = new ShopForm();
                            shop.Show();
                            shop.AdminRights();
                            this.Hide();
                        }
                        else
                        {
                            var shop = new ShopForm();
                            shop.Show();
                            shop.UserRights();
                            this.Hide();
                        }
                    }
                }
                else
                {
                    txtUsername.Text = null;
                    txtPassword.Text = null;
                }

            }
        }
    }
}
