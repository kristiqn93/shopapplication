﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace LoginForm
{
    public partial class RegisterForm : DevExpress.XtraEditors.XtraForm
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (var db = new ShopStorageDatabase())
            {
                var user = db.Users.Create();
                if (txtPassoword.Text == txtRepeatPass.Text &&
                    txtUsername.Text.Length > 3 && txtEmail.Text.Length > 3 &&
                    txtFirstName.Text.Length >3 && txtLastName.Text.Length > 3 &&
                    txtPassoword.Text.Length > 3)
                {
                    user.Username = txtUsername.Text;
                    user.Password = txtPassoword.Text;
                    user.Email = txtEmail.Text;
                    user.FirstName = txtFirstName.Text;
                    user.LastName = txtLastName.Text;
                    
                    if (rbtnAdmin.Checked == true)
                    {
                        user.RIghts = "admin";
                    }
                    else
                    {
                        user.RIghts = "user";
                    }
                    db.Users.Add(user);
                    db.SaveChanges();

                    this.Close();
                    MessageBox.Show("Account was added succefull.", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                }
                else
                {
                    txtUsername.Text = null;
                    txtEmail.Text = null;
                    txtFirstName.Text = null;
                    txtPassoword.Text = null;
                    txtRepeatPass.Text = null;
                    txtLastName.Text = null;

                    MessageBox.Show("Wrong entered fields.", "Error !", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }
    }
}